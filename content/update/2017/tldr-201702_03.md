+++
tags = "tl;dr"
date = "2017-02-18T14:22:10-03:00"
title = "TL;DR Febrero 2017, 3ra semana"
tipo = "tl;dr"

+++

# TL;DR Febrero 2017, 3ra semana

https://arstechnica.com/security/2017/02/now-sites-can-fingerprint-you-online-even-when-you-use-multiple-browsers/  
Algoritmo para generar un fingerprint asociado al computador que se puede trackear en los distintos navegadores instalados. Aparte de las características propias del browser (configuraciones, plugins, etc.) revisa componentes que se usan en todos los navegadores: uso de tarjetas gráficas, tarjetas de audio, cantidad de cores de procesador y fuentes de texto instaladas. Reducir características de WebGL en el navegador puede ayudar a dificultar la generación de un fingerprint único para el computador.

https://medium.com/amnesty-insights/operation-kingphish-uncovering-a-campaign-of-cyber-attacks-against-civil-society-in-qatar-and-aa40c9e08852  
Campaña de phishing en contra de activistas relacionados a la explotación laboral de migrantes de Nepal en Qatar. En vez de usar correos para el phishing, la campaña usa una "persona" con presencia en redes sociales y que se vincula (sigue o es amiga de) conocidos de las víctimas, de esa manera genera un puente por el cual comienza a chatear (durante meses en algunos casos) con las víctimas y desde donde envía la URL del phising. El phishing en particular consta de una página que simula el login personalizado de Google (nombre e imagen de perfil de la víctima) para luego redireccionar a un documento compartido en Drive (simulando una experiencia real de login y acceso a un documento compartido).

https://arstechnica.com/security/2017/02/new-aslr-busting-javascript-is-about-to-make-drive-by-exploits-much-nastier/  
Nueva técnica, ASLR⊕Cache o AnC, que permite sobrepasar ASLR utilizando JavaScript para atacar la unidad de manejo de memoria (MMU) del procesador. Usa una técnica llamada EVICT + TIME cache attack para descubrir las direcciones de memoria necesarias para poder explotar vulnerabilidades y ejecutar código propio. A diferencia de otras técnicas, ASLR Cache es independiente del SO y tiene que ver más con el diseño del CPU, donde el funcionamiento del Cache funciona en contra del propósito de ASLR. Intel, AMD y ARM son vulnerables a este ataque. De momento no existe ningún tipo de medida correctiva o paliativa y el consejo es a no usar ASLR como protección primaria.  
Paper: http://www.cs.vu.nl//~herbertb/download/papers/anc_ndss17.pdf

https://riseup.net/en/about-us/press/canary-statement  
Enfrentando la decisión entre cerrar el proyecto y colaborar con dos ordenes para entregar información sobre cuentas de e-mail por parte del FBI, Riseup decide cooperar y al mismo tiempo decide no tener que estar en la misma posición nuevamente, por lo cual implementaron un sistema de encriptación para almacenar los correos electrónicos en sus servidores que les imposibilita acceder al contenido de los mismos. La solución, Trees, funciona como un plugin de Dovecot que utiliza NaCL para cifrar con la contraseña del usuario la llave de desencriptación.  
Trees: https://0xacab.org/riseuplabs/trees

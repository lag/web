+++
date = "2017-02-23T22:09:37-03:00"
title = "Error en Cloudflare significa cambia tus contraseñas"
tipo = "txt"

+++

## ¿Qué pasó?

Cloudflare es una empresa que actúa como punto intermedio cuando te conectas a distintos sitios web. En el papel, Cloudflare es una red de entrega de contenidos, cuya función es hacer que consigas acceder con mayor rapidez a distintos servicios web que le ocupan. Sin embargo el tener a un tercero como intermediario no es siempre una buena idea.

La semana pasada Tavis Ormandy de Google Project Zero encontró de forma accidental información confidencial al hacer búsquedas en Google[^1]. Cloudflare se encontraba repartiendo de forma abierta información confidencial, incluyendo sesiones protegidas por HTTPS y esta incluso la estaba almacenando distintos buscadores en sus cachés.

¿Qué tan grave es? Si usaste un servicio que es cliente de Cloudflare tus contraseñas, direcciones IP, cookies, comentarios, información de salud, mensajes privados, todo quedó expuesto en internet para cualquiera entre el 22 de septimbre de 2016 y 20 de febrero de 2017. Aún es posible obtener dicha información en revisando el caché de buscadores.

La respuesta al incidente por parte de Cloudflare ha sido por lo bajo terrible. 

* Cloudflare lleva un programa de premios por reportar fallos, este crítico merecía una polera. 
* Aún cuando está el caché de Google indicando que el problema lleva meses, están deliberadamente mintiendo indicando que solo duró unos días. [^2]
* También mienten al indicar que el registro fue eliminado del caché de los buscadores [^3]

## Medidas a tomar

Se aproxima el fin de semana, al ser tan grande el número de sitios que usan Cloudflare, por favor tómate un momento para revisar qué servicios fueron afectados y cambiar tus contraseñas. Lamentablemente lo primero no es tarea fácil.

Acá una lista rápida de servicios comprometidos:

* OKCupid
* Uber
* 1password
* Reddit
* Yelp
* Pingdom
* Digital Ocean
* Rap Genious
* Udemy
* Coinbase
* Product Hunt
* Fitbit
* Stack Overflow
* Hacker news
* Zendesk
* Crunchy roll
* Feedly

Como la lista es incierta, puede ser una buena idea que rotes todas tus contraseñas... y lo más importante usa una contraseña por sitio y un gestor de contraseñas. 

Idealmente los sitios afectados deberían informar a sus usuarios y forzar el cambio de contraseña, pero de ahí a que eso suceda puede tardar meses.

Ah, y al igual que con heartbleed[^4] al exponerse las sesiones la autenticación en dos pasos no sirve como medida precautoria.[^5]

## Actualización

Alguien armó una lista con los servicios afectado en https://github.com/pirate/sites-using-cloudflare. Actualmente la lista posee 4,287,625 sitios, así que puede ser algo lenta la búsqueda.


[^1]: Reporte https://bugs.chromium.org/p/project-zero/issues/detail?id=1139

[^2]: A https://twitter.com/pmoust/status/834916647873961984, B https://twitter.com/taviso/status/834918182640996353

[^3]: "External impact and cache clearing" https://blog.cloudflare.com/incident-report-on-memory-leak-caused-by-cloudflare-parser-bug/#externalimpactandcacheclearing

[^4]: https://es.wikipedia.org/wiki/Heartbleed

[^5]: cookies las que se filtran, de nada sirve la autenticación en dos pasos https://it.slashdot.org/story/14/04/19/174227/heartbleed-used-to-bypass-2-factor-authentication-hijack-user-sessions 

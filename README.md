# L(A)G

## Instrucciones

```
sudo apt install hugo
git clone git@0xacab.org:lag/web.git lag
cd lag
hugo
```

Si no arroja ningún error, estamos.

### Creando contenido

Reemplaza {título} por el que busques y {editor} por emacs, vim, nano, etc...

```
git checkout -b {titulo}
hugo new update/{año}/{titulo}.md 
{editor} content/update/{año}/{titulo}.md
git add content/update/{año}/{titulo}.md
git commit -m "primera versión de {titulo}"
git push
```

## Licencia

gplv3 o barbarie

## Contacto

#etp en irc.indymedia.org
